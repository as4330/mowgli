#include<iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <stdlib.h>
using namespace std;

template <typename T>
string NumberToString(T pNumber)
{
     ostringstream oOStrStream;
      oOStrStream << pNumber;
       return oOStrStream.str();
}

template<typename Out>
void split(string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}


vector<string> split(string &s, char delim) {
    vector<string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

int main(){
	string line; 
	ifstream infile("/home/opim/amansin/appDownloadRank.csv");
	int last_app_id = -1;
    ofstream ofile;
    ofile.open("/home/opim/amansin/AppMarket/tmp.csv" );
    vector<int> col1 ; 
    while (getline(infile,line) ) {
        vector<string> tokens ; 
        tokens = split(line,',');
        int current_app_id = atoi(tokens[0].c_str());
        col1.push_back(current_app_id);

	}
    cout<<col1.size();
}
